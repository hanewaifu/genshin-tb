# Maintener : Hane VAILLARD
import json
import os
import requests
from django.http import HttpResponse


def get_chars():
    resp = requests.get("https://genshin-api.santospatrick.com/api/v1/characters")
    return resp


def index(request):
    return HttpResponse("Hello world")

def chars(request):
    return HttpResponse("Characters page")

def main():
    return "Hello world"

if __name__ == "__main__":
    # execute only if run as a script
    main()
